# Acoustic Modem Message Protocol

This repository contains a Go package to implement a simple messaging protocol for use with Acoustic Modems. The protocol is packet based. Each packet has the following structure:

| **Starting Byte** | **Size** | **Description**            |
|-------------------|----------|----------------------------|
| 0                 | 1        | Start delimiter, "<"       |
| 1                 | 1        | Sequence number            |
| 2                 | 2        | Data length, N (LSB first) |
| 4                 | N        | Data                       |
| N+4               | 1        | CRC-8                      |
| N+5               | 1        | End delimiter, ">"         |

## Bulk-data Transfer

A bulk-data transfer protocol is layered on top of the message packets. This protocol is implemented by the Sender and Receiver types.
