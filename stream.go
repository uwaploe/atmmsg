package atmmsg

import (
	"context"
	"errors"
	"io"
	"sync"
	"time"
)

// MsgType is stored in the first byte of every packet sent
type MsgType byte

const (
	MsgAck   MsgType = 0x55 // Packet acknowledgement
	MsgNak           = 0xaa // Negative acknowledgement
	MsgData          = 0x01 // Data packet
	MsgEot           = 0x81 // End of transmission
	MsgAbort         = 0xff // Abort transmission
)

var (
	ErrAbort      = errors.New("Aborted by receiver")
	ErrIncomplete = errors.New("Incomplete data transfer")
)

// Receiver implements the receive side of a sliding-window
// bulk-data transfer protocol.
type Receiver struct {
	rw       io.ReadWriter
	winSize  byte
	lfr, laf byte
	window   [][]byte
	nHeld    int
	enc      *Encoder
}

// NewReceiver creates a Receiver attached to rw
func NewReceiver(rw io.ReadWriter) *Receiver {
	return &Receiver{
		rw:      rw,
		winSize: 1,
		enc:     NewEncoder(rw),
	}
}

func (r *Receiver) sendNak(seq byte) error {
	r.enc.SetSeq(seq)
	_, err := r.enc.Write([]byte{MsgNak})
	return err
}

func (r *Receiver) sendAck(seq byte) error {
	r.enc.SetSeq(seq)
	_, err := r.enc.Write([]byte{byte(MsgAck)})
	return err
}

func (r *Receiver) processWindow(seq byte, block []byte, w io.Writer) error {
	if block != nil {
		r.window[seq-(r.lfr+1)] = block
		r.nHeld++
	}

	var (
		i, n int
	)

	n = int(r.winSize)
	if seq == (r.lfr + 1) {
		// Packet stored in the first window slot. Send all packets
		// from the head of the window to the Writer.
		for i = 0; i < n; i++ {
			if r.window[i] != nil {
				if _, err := w.Write(r.window[i]); err != nil {
					return err
				}
				r.window[i] = nil
				r.nHeld--
			} else {
				break
			}
		}
		// Shift any remaining packets down toward the head of the window
		shift := i
		for ; i < n; i++ {
			r.window[i-shift] = r.window[i]
			r.window[i] = nil
		}
		r.lfr += byte(shift)
		r.laf = r.lfr + r.winSize
	}

	return nil
}

// Receive streams data from a Sender to an io.Writer. If ch is non-nil, the
// size of each incoming packet will be sent to it.
func (r *Receiver) Receive(ctx context.Context, w io.Writer, ch chan<- int) error {
	r.window = make([][]byte, int(r.winSize))
	r.nHeld = 0
	r.lfr = 255
	r.laf = r.lfr + r.winSize

loop:
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		payload, seq, err := ReadRawPacketCtx(ctx, r.rw)
		if err == ErrFraming || err == ErrCrc {
			time.Sleep(time.Second * 2)
			r.sendNak(seq)
			continue
		}

		if err != nil {
			return err
		}

		if ch != nil {
			select {
			case ch <- len(payload) - 1:
			default:
			}
		}

		switch payload[0] {
		case MsgEot:
			if TraceFunc != nil {
				TraceFunc("RECV: EOT")
			}
			r.processWindow(seq, nil, w)
			break loop
		case MsgData:
			time.Sleep(time.Second * 2)
			r.sendAck(seq)
			if (r.laf - seq) < r.winSize {
				r.processWindow(seq, payload[1:], w)
				if TraceFunc != nil {
					TraceFunc("RECV: LFR=%d LAF=%d N=%d", r.lfr, r.laf, r.nHeld)
				}
			}
		}

	}

	if r.nHeld > 0 {
		return ErrIncomplete
	}

	return nil
}

type pktState struct {
	seq      byte
	deadline time.Time
	data     []byte
	retry    int
}

// Sender implements the send side of a sliding-window
// bulk-data transfer protocol.
type Sender struct {
	rw       io.ReadWriter
	enc      *Encoder
	winSize  int
	lar, lfs byte
	window   map[byte]pktState
	mu       *sync.Mutex
	timeout  time.Duration
	retries  int
	pktSize  uint
}

// NewSender returns a Sender connected to rw
func NewSender(rw io.ReadWriter) *Sender {
	s := &Sender{
		rw:      rw,
		winSize: 1,
		mu:      &sync.Mutex{},
		retries: 1,
		pktSize: 255,
	}
	s.enc = NewEncoder(s.rw)
	return s
}

// SetRetries sets the number of send attempts when no ACK has been received.
func (s *Sender) SetRetries(n int) {
	s.retries = n
}

// SetPktSize sets the maximum number of bytes to send in each packet
func (s *Sender) SetPktSize(n uint) error {
	if n == 0 || n > MaxPacketLen {
		return ErrSize
	}
	s.pktSize = n
	return nil
}

func (s *Sender) handleAck(seq byte) {
	s.mu.Lock()
	defer s.mu.Unlock()

	if _, ok := s.window[seq]; ok {
		delete(s.window, seq)
		s.lar = seq
	}
}

func (s *Sender) resend(seq byte) {
	s.mu.Lock()
	defer s.mu.Unlock()

	if p, ok := s.window[seq]; ok {
		save_seq := s.enc.Seq()
		s.enc.SetSeq(seq)
		s.enc.Write(p.data)
		s.enc.SetSeq(save_seq)
		p.retry--
		if p.retry < 0 {
			delete(s.window, seq)
		} else {
			p.deadline = time.Now().Add(s.timeout)
			s.window[seq] = p
		}
	}
}

func (s *Sender) storePacket(seq byte, block []byte) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.window[seq] = pktState{
		seq:      seq,
		data:     block,
		retry:    s.retries,
		deadline: time.Now().Add(s.timeout)}
}

func (s *Sender) processWindow() {
	seqs := make([]byte, 0)
	s.mu.Lock()
	t := time.Now()
	for seq, p := range s.window {
		if p.deadline.Before(t) {
			seqs = append(seqs, seq)
		}
	}
	s.mu.Unlock()
	for _, seq := range seqs {
		s.resend(seq)
	}
}

func (s *Sender) waitForAck(r io.Reader) error {
	payload, seq, err := ReadRawPacket(r)
	if err != nil || payload == nil {
		return nil
	}

	switch MsgType(payload[0]) {
	case MsgAck:
		s.handleAck(seq)
		if TraceFunc != nil {
			TraceFunc("SEND: LAR=%d LFS=%d waiting=%d",
				s.lar, s.lfs, len(s.window))
		}
	case MsgNak:
		s.resend(seq)
	case MsgAbort:
		return ErrAbort
	}

	return nil
}

// Send streams data from an io.Reader to a Receiver
func (s *Sender) Send(ctx0 context.Context, r io.Reader,
	ackTimeout time.Duration) error {
	s.window = make(map[byte]pktState)
	s.enc.SetSeq(0)
	s.lar = 255
	s.lfs = 255
	s.timeout = ackTimeout

	for {
		if len(s.window) < s.winSize {
			buf := make([]byte, s.pktSize)
			buf[0] = MsgData
			n, _ := r.Read(buf[1:])
			if n == 0 {
				break
			}
			seq := s.enc.Seq()
			s.enc.Write(buf[:n+1])
			s.storePacket(seq, buf[:n+1])
			s.lfs = seq
			if TraceFunc != nil {
				TraceFunc("SEND: LAR=%d LFS=%d waiting=%d",
					s.lar, s.lfs, len(s.window))
			}
		} else {
			err := s.waitForAck(s.rw)
			if err != nil {
				return err
			}
			s.processWindow()
		}

		select {
		case <-ctx0.Done():
			return ctx0.Err()
		default:
		}
	}

	for len(s.window) > 0 {
		err := s.waitForAck(s.rw)
		if err != nil {
			return err
		}
		select {
		case <-ctx0.Done():
			return ctx0.Err()
		default:
			s.processWindow()
		}
	}

	s.enc.Write([]byte{MsgEot})

	return nil
}
