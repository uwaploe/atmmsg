package atmmsg

import (
	"bytes"
	"context"
	"crypto/rand"
	"io"
	"net"
	"testing"
	"time"

	"github.com/ian-kent/linkio"
)

func TestReadWrite(t *testing.T) {
	var buf bytes.Buffer

	w := NewEncoder(&buf)
	w.SetSeq(42)
	payload := []byte("hello world")
	n, err := w.Write(payload)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("buf: %q", buf.Bytes())

	if n != len(payload) {
		t.Errorf("Bad data length; expected %d, got %d", len(payload), n)
	}

	pkt, seq, err := ReadRawPacket(&buf)
	if err != nil {
		t.Fatal(err)
	}

	if seq != 42 {
		t.Errorf("Bad sequence number; expected 42, got %d", seq)
	}

	if string(pkt) != string(payload) {
		t.Errorf("Data error; expected %q, got %q", payload, pkt)
	}
}

const BaudRate = 240000

type wrappedRW struct {
	r io.Reader
	w io.Writer
}

func (w *wrappedRW) Read(p []byte) (int, error) {
	return w.r.Read(p)
}

func (w *wrappedRW) Write(p []byte) (int, error) {
	return w.w.Write(p)
}

func wrapRW(rw io.ReadWriter, speed int) *wrappedRW {
	link := linkio.NewLink(linkio.Throughput(speed) * linkio.BitPerSecond)
	return &wrappedRW{
		r: link.NewLinkReader(rw),
		w: link.NewLinkWriter(rw),
	}
}

func startReceiver(t *testing.T, ctx context.Context, w io.Writer) (net.Listener, chan struct{}) {
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal(err)
	}

	done := make(chan struct{}, 1)
	go func() {
		conn, err := listener.Accept()
		defer close(done)
		if err != nil {
			t.Fatal(err)
		}
		r := NewReceiver(wrapRW(conn, BaudRate))
		err = r.Receive(ctx, w, nil)
		if err != nil {
			t.Error(err)
		}
	}()

	return listener, done
}

func TestStream(t *testing.T) {
	buf := make([]byte, 2048)
	rand.Read(buf)

	src := bytes.NewReader(buf)
	var sink bytes.Buffer

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	if testing.Verbose() {
		TraceFunc = t.Logf
	}

	l, done := startReceiver(t, ctx, &sink)
	defer l.Close()

	addr := l.Addr().String()
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		t.Fatal(err)
	}
	s := NewSender(wrapRW(conn, BaudRate))
	s.SetPktSize(1024)

	err = s.Send(ctx, src, time.Second*2)
	if err != nil {
		t.Fatal(err)
	}

	select {
	case <-done:
	case <-time.After(time.Second * 2):
		cancel()
	}

	t.Logf("%d bytes transferred", sink.Len())

	if sink.String() != string(buf) {
		t.Error("Data mismatch")
	}
}
