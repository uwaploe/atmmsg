// Package protocol implements the packet-based communication protocol used over
// the acoustic modem link
package atmmsg

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
)

const (
	StartDelimiter byte = 0x3c // "<"
	EndDelimiter        = 0x3e // ">"
)
const MaxPacketLen = 65535

// TraceFunc is used to log the contents of incoming and outgoing packets
var TraceFunc func(string, ...interface{})

var (
	ErrCrc     = errors.New("CRC check failed")
	ErrFraming = errors.New("Framing error")
	ErrSize    = errors.New("Invalid payload size")
)

func readAll(rdr io.Reader, p []byte) (int, error) {
	var (
		err   error
		n, nn int
	)

	total := len(p)
	for n < total {
		nn, err = io.ReadFull(rdr, p[n:])
		n += nn
		if nn == 0 && err != nil {
			break
		}
	}

	return n, err
}

// ReadPacket returns the payload of the next packet from an io.Reader
func ReadRawPacket(rdr io.Reader) ([]byte, byte, error) {
	var (
		crc, crc_check, seq byte
		plen                int
	)

	char := make([]byte, 1)
	// Scan for a valid start delimiter
	for {
		n, err := rdr.Read(char)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}
		if err != nil {
			return nil, seq, fmt.Errorf("looking for START: %w", err)
		}
		if TraceFunc != nil {
			TraceFunc("IN: %q", char)
		}
		if char[0] == StartDelimiter {
			break
		}
	}

	// Read the sequence number
	_, err := rdr.Read(char)
	if err != nil {
		return nil, seq, fmt.Errorf("reading SEQ: %w", err)
	}
	crc_check = Crc8(char, crc_check)
	seq = char[0]

	// Read the packet length bytes (LSB first)
	plen = 0
	for i := 0; i < 2; i++ {
		_, err = rdr.Read(char)
		if err != nil {
			return nil, seq, fmt.Errorf("reading size: %w", err)
		}
		crc_check = Crc8(char, crc_check)
		plen += int(char[0]) << (uint(i) * 8)
	}

	payload := make([]byte, plen)
	_, err = readAll(rdr, payload)
	if err != nil {
		return nil, seq, fmt.Errorf("reading payload: %w", err)
	}

	if TraceFunc != nil {
		n := len(payload)
		trailer := ""
		if n > 20 {
			n = 20
			trailer = "..."
		}
		TraceFunc("IN: [%d, %d, %q%s]\n", seq, plen, payload[0:n], trailer)
	}

	// Read CRC
	_, err = rdr.Read(char)
	if err != nil {
		return nil, seq, fmt.Errorf("reading CRC: %w", err)
	}
	crc = char[0]

	// Read terminating delimiter
	_, err = rdr.Read(char)
	if err != nil {
		return nil, seq, fmt.Errorf("reading TERM: %w", err)
	}

	if char[0] != EndDelimiter {
		return nil, seq, ErrFraming
	}

	// Check CRC
	crc_check = Crc8(payload, crc_check)
	if crc != crc_check {
		return nil, seq, ErrCrc
	}

	return payload, seq, nil
}

// ReadPacketCtx is like ReadRawPacket but includes a Context. This function will
// ignore EOF errors and will return when either a complete packet is read or
// the Context is cancelled.
func ReadRawPacketCtx(ctx context.Context, rdr io.Reader) ([]byte, byte, error) {
	var (
		payload []byte
		seq     byte
		err     error
	)

	for {
		select {
		case <-ctx.Done():
			return payload, seq, ctx.Err()
		default:
		}
		payload, seq, err = ReadRawPacket(rdr)
		if err != nil && TraceFunc != nil {
			TraceFunc("Read error [%d]: %v", seq, err)
		}
		if errors.Is(err, io.EOF) || errors.Is(err, os.ErrDeadlineExceeded) {
			continue
		}
		break
	}

	return payload, seq, err
}

// Encoder wraps an io.Writer and packs the contents
// of every Write into a packet.
type Encoder struct {
	w   io.Writer
	seq byte
}

// NewEncoder returns an Encoder
func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w: w}
}

// Seq returns the sequence number for the next packet.
func (e *Encoder) Seq() byte {
	return e.seq
}

// SetSeq sets the sequence number for the next packet.
func (e *Encoder) SetSeq(val byte) {
	e.seq = val
}

// Write writes the contents of p as a single packet. The maximum size of p
// is 65535 bytes. The sequence number is incremented if the Write succeeds.
func (e *Encoder) Write(p []byte) (int, error) {
	if len(p) > MaxPacketLen {
		return 0, ErrSize
	}

	char := []byte{0}
	start := StartDelimiter
	end := EndDelimiter

	// Write the start delimiter
	char[0] = byte(start)
	e.w.Write(char)

	var crc_check byte

	// Write the sequence number
	char[0] = e.seq
	crc_check = Crc8(char, crc_check)
	e.w.Write(char)

	// Write the payload length (LSB first) ...
	plen := len(p)
	char[0] = byte(plen)
	crc_check = Crc8(char, crc_check)
	e.w.Write(char)
	plen = plen >> 8
	char[0] = byte(plen)
	crc_check = Crc8(char, crc_check)
	e.w.Write(char)

	// ... and the payload
	n, err := e.w.Write(p)
	if err != nil {
		return n, err
	}

	if TraceFunc != nil {
		n := len(p)
		trailer := ""
		if n > 20 {
			n = 20
			trailer = "..."
		}
		TraceFunc("OUT: [%d, %d, %q%s]\n", e.seq, len(p), p[0:n], trailer)
	}

	// Write the CRC and terminating delimiter
	char[0] = Crc8(p, crc_check)
	_, err = e.w.Write(char)
	if err != nil {
		return n, err
	}

	char[0] = byte(end)
	_, err = e.w.Write(char)
	if err != nil {
		return 0, err
	}
	e.seq++

	return n, nil
}
